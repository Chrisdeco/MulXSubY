package multosub;

import java.awt.*;
import java.awt.Font;
import java.awt.event.*;

import javax.swing.*;

public class GUI extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private Calculator calculator;
	private JPanel panel = new JPanel();
	
	private JLabel Getal1 = new JLabel("startgetal");
	private JTextField txtFieldgetal1 = new JTextField();
	
	private JLabel Getal2 = new JLabel("eindgetal");
	private JTextField txtFieldgetal2 = new JTextField();
	
	private JLabel multiplier = new JLabel("multiplierX");
	private JTextField txtFieldMultiplier = new JTextField();
	
	private JLabel substractor = new JLabel("substractorY");
	private JTextField txtFieldSubstractor = new JTextField();
	
	private JButton berekenKnop = new JButton("Bereken");
	private JLabel numberOfCalculations = new JLabel("Number of calculations:");
	private JLabel values = new JLabel("Values:");
	private JLabel numberOfMult = new JLabel("Number of multiplications:");
	private JLabel numberOfSubs = new JLabel("Number of substractions:");
	private JLabel errorMessage = new JLabel("Not all inputs were integers!");
	public GUI() {
		
		setTitle("MultXSubsYGame");
		setBounds(250, 250, 600, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Font font = new Font("arial",Font.BOLD,22);
		
		Getal1.setBounds(10,10,100,15);
		txtFieldgetal1.setBounds(120,10,50,15);
		
		Getal2.setBounds(10,40,100,15);
		txtFieldgetal2.setBounds(120,40,50,15);
		
		multiplier.setBounds(10,70,100,15);
		txtFieldMultiplier.setBounds(120,70,50,15);
		
		substractor.setBounds(10,100,100,15);
		txtFieldSubstractor.setBounds(120,100,50,15);
		
		berekenKnop.addActionListener(this);
		berekenKnop.setBounds(200, 130, 100, 20);
		
		numberOfCalculations.setBounds(10, 160 , 700 ,15);
		values.setBounds(10,190,700,15);
		
		numberOfMult.setBounds(10,220,350,15);
		numberOfSubs.setBounds(10,250,350,15);
		
		errorMessage.setBounds(10, 280, 300, 25);
		errorMessage.setVisible(false);
		errorMessage.setFont(font);
		errorMessage.setForeground(Color.RED);
		add(panel);
		panel.add(txtFieldgetal1);
		panel.add(txtFieldgetal2);
		panel.add(berekenKnop);
		panel.add(Getal1);
		panel.add(Getal2);
		panel.add(multiplier);
		panel.add(txtFieldMultiplier);
		panel.add(substractor);
		panel.add(txtFieldSubstractor);
		panel.add(numberOfCalculations);
		panel.add(values);
		panel.add(numberOfMult);
		panel.add(numberOfSubs);
		panel.add(errorMessage);
		panel.setLayout(null);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		String textFieldGetal1 = txtFieldgetal1.getText();
		String textFieldGetal2 = txtFieldgetal2.getText();
		String textFieldX = txtFieldMultiplier.getText();
		String textFieldY = txtFieldSubstractor.getText();
		if( isInt(textFieldGetal1)== true && isInt(textFieldGetal2) ==true
				&& isInt(textFieldX) ==true && isInt(textFieldY) ==true) {
			errorMessage.setVisible(false);
		int a = Integer.parseInt(textFieldGetal1);
		int b = Integer.parseInt(textFieldGetal2);
		int c = Integer.parseInt(textFieldX);
		int d = Integer.parseInt(textFieldY);
		calculator = new Calculator(a,b,c,d);
		calculator.mulXSubY();
		numberOfMult.setText(" Number of multiplications: " +calculator.getNumberOfMultip());
		numberOfSubs.setText(" Number of substractions: "+calculator.getNumberOfSubstr());
		values.setText("Values: "+ calculator.getOutput());
		numberOfCalculations.setText(" Number of calculations: "+ calculator.getNumberOfOperations());
	}
	 	else{
	 		errorMessage.setVisible(true);
	 	}
	 	}
	public boolean isInt(String s) {
	
	 try
	  { int i = Integer.parseInt(s); return true; }

	 catch(NumberFormatException er)
	  { return false; }
	}

	public static void main(String[] args) {
		new GUI();
	}
}