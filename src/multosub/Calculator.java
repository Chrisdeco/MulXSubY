package multosub;

public class Calculator {
	private int getal1;
	private int getal2;
	private int numberOfMultip;
	private int numberOfSubstr;
	private int numberOfOperations;
	private int multiplier;
	private int substractor;
	private int[] array1 = new int[102];
	private int[] array2 = new int[102];
	private int index1 = 0;
	private int index2 = 0;
	private int origGetal1;
	private String output;
	private boolean initialSmallerThan;
	private boolean equals1;
	private boolean equals2;
	// private Tree<> tree;

	public Calculator(int x, int y, int a, int b) {
		getal1 = x;
		origGetal1= x;
		getal2 = y;
		multiplier = a;
		substractor = b;
		array1[index1] = x;
		array2[index2] = x;
		index1++;
		index2++;
	}

	public void mulXSubY() {

		if (getal1 < getal2) {
			initialSmallerThan = true;
		} else {
			initialSmallerThan = false;
		}

		while (getal1 != getal2 && index1 < 100) {

			if (initialSmallerThan == true) {
				int a = getal1 * multiplier;
				getal1 = a;
				index1++;
				array1[index1] = getal1;
				numberOfMultip++;
				numberOfOperations++;
				if (getal1 == getal2) {
					
				} else {
					int b = getal1 - substractor;
					getal1 = b;
					index1++;
					array1[index1] = getal1;
					numberOfSubstr++;
					numberOfOperations++;
				}
			}

			else {
				int a = getal1 - substractor;
				getal1 = a;
				index1++;
				array1[index1] = getal1;
				numberOfSubstr++;
				numberOfOperations++;
				if (getal1 == getal2) {
				} else {
					int b = getal1 * multiplier;
					getal1 = b;
					index1++;
					array1[index1] = getal1;
					numberOfMultip++;
					numberOfOperations++;
				}
			}
		}
			
		while (getal1 != getal2 && index2 < 100) {
				getal1 = origGetal1;
			if (initialSmallerThan == true) {
				
				int a = getal1 * multiplier;
				getal1 = a;
				index2++;
				array2[index2] = getal1;
				numberOfMultip++;
				numberOfOperations++;
				if (getal1 == getal2) {
				} else {
					int b = getal1 - substractor;
					getal1 = b;
					index2++;
					array2[index2] = getal1;
					numberOfSubstr++;
					numberOfOperations++;
				}
			}

			else {
				int a = getal1 - substractor;
				getal1 = a;
				index2++;
				array2[index2] = getal1;
				numberOfSubstr++;
				numberOfOperations++;
				if (getal1 == getal2) {
				} else {
					int b = getal1 * multiplier;
					getal1 = b;
					index2++;
					array2[index2] = getal1;
					numberOfMultip++;
					numberOfOperations++;
				}

			}
		}
			if (getal1 == getal2) {
				if (numberOfOperations == 0) {
					output = " Beide getallen zijn gelijk, er zijn geen bewerkingen nodig";
				} else {
					output = toStringTussenValues();
				}
			} else {
				output = " Er is geen oplossing gevonden";
			}

		}
	

	/*
	 * public void mulXSubY() { while (getal1 != getal2 && index < 101) {
	 * 
	 * if (getal1 < getal2) { if (getal1 > 0.5*getal2 -substractor) { int a = getal1
	 * - substractor; getal1 = a; numberOfSubstr++; numberOfOperations++;
	 * array[index] = getal1; index++;
	 * 
	 * }
	 * 
	 * 
	 * else { int a = getal1 * multiplier; getal1 = a; numberOfMultip++;
	 * numberOfOperations++; array[index] = getal1; index++; } } else if (getal1 >
	 * getal2) { int a = getal1 - substractor; getal1 = a; numberOfSubstr++;
	 * numberOfOperations++; array[index] = getal1; index++; } } if (getal1 ==
	 * getal2) { if (numberOfOperations == 0) { output =
	 * " Beide getallen zijn gelijk, er zijn geen bewerkingen nodig"; } else {
	 * output = toStringTussenValues(); } } else { output =
	 * " Er is geen oplossing gevonden"; }
	 * 
	 * }
	 */

	public String toStringTussenValues() {
		StringBuffer tussenResults = new StringBuffer();
		for (int i = 0; i < 101; i++) {
			tussenResults.append(" ");
			if (array1[i] == 0) {
				tussenResults.append("");
			} else {
				tussenResults.append(array1[i]);
				tussenResults.append(" ");
			}
		}

		return tussenResults.toString();
	}

	public String getOutput() {
		return output;
	}

	public int getNumberOfSubstr() {
		return numberOfSubstr;
	}

	public int getNumberOfMultip() {
		return numberOfMultip;
	}

	public int getNumberOfOperations() {
		return numberOfOperations;
	}

	public void setGetal1(int x) {
		getal1 = x;
	}

	public void setGetal2(int y) {
		getal2 = y;
	}

}
