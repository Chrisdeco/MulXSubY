Project MulXSubY

EDIT:( After finishing this, I realized I only did a part of what I had to do, I was confused by the one example
(which works)
and thought the only way this application had to work, was to constantly switch between multiplication
and substracting.
I will try to finish this correct manner ASAP
I know this will go via a tree where the root is *getal1*, each node of the tree will have 2 children, one will
have the value after multiplying *getal1* with X, the other one the value after Substracting.
Each number stored in the child nodes will undergo the same process as the root. After each step we check if 
we have reached *getal2*. If this is the case , we will go up until we reach the Root, this way we will have found 
the shortest way to get from *getal1* to *getal2*.)
    
This is a readme of the code I handed in on may 6th.

*getal1* and *getal2* are the numbers of which you want the quickest way to get from one to the other.

Describing public method mulXSubY
At first we set everything at zero/false, just to make sure previous calculations won't have any effect on a newly executed one

Thereafter we check which one of our numbers is the smallest, if *getal1* < *getal2*  boolean *initialSmallerThan* is set to true. We will use this later (1*)

Now we have arrived at the while loops, one will start multiplying, the second will start substracting if (1*) is true,
this is the other way around if (1*) is false


The first while loop will start multiplying followed by substracting if (1*) is true,
or else it will start with substracting followed by multiplication
After each multiplication or substraction we check if getal1 == getal2, if this is the case the while loop
will be stopped.

If *getal1* == *getal2* *equals1* will be set to true;
if this is the case, the second while loop won't be used.
Then we check again if *getal1*==*getal2* (just in case *equals1* wasn't true and we had to use the second loop. In 
the second loop getal1 gets a reset to the inputvalue, stored in variable *origGetal1*)

We give variable *output* a special value in case both numbers were the same at the very beginning , in this case neither of
the 2 While loops will have been used so both numberOfOperationCounters will be 0.
If the numberOfOperationCounters aren't 0, we check with what while loop we reached *getal2* starting from *getal1*
then we get the array linked to While1 or while2 and convert it to a string which will be displayed on our GUI.

In case no solution was found (meaning *getal1* wasn't converted to *getal2* within a hundred operations) 
we return that there were no solutions.










